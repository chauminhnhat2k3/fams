package com.example.fams.models;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tblClassUser")
public class ClassUser {

    @ManyToOne
    private User userId;
    @ManyToOne
    private Class classId;
    private String userType;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCU;
}
