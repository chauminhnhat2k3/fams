package com.example.fams.models;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tblTrainingProgramSyllabus")
public class TrainingProgramSyllabus {
    @ManyToOne
    public Syllabus topicCode;
    @ManyToOne
    public TrainingProgram trainingProgramCode;
    public String sequence;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTPS;
}
