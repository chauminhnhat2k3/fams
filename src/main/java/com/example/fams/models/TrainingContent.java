package com.example.fams.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tblTrainingContent")
public class TrainingContent {
    private String content;
    @ManyToOne
    private LearningObjective learningObjective;
    private String type;
    private String deliveryType;
    private Integer duration;
    private String trainingFormat;
    private String note;
    @ManyToOne
    private TrainingUnit unitCode;
    @Id
    private Long id;
}
