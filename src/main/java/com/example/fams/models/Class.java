package com.example.fams.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tblClass")
public class Class {

    @Id
    private Long classId;
    private String trainingProgramCode;
    private String className;
    private String classCode;
    private Integer duration;
    private String status;
    private String location;
    private String fsu;
    private LocalDate startDate;
    private LocalDate endDate;
    private String createBy;
    private LocalDate createDate;
    private String modifiedBy;
    private LocalDate modifiedDate;
}
