package com.example.fams.models;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tblSyllabusObjective")
public class SyllabusObjective {
    @ManyToOne
    private Syllabus topicCode;
    @ManyToOne
    private LearningObjective objectiveCode;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSO;
}
