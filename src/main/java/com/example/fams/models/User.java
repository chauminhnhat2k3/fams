package com.example.fams.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tblUser")
public class User {

    @Id
    private Long userId;
    private String name;
    private String email;
    private String phone;
    private LocalDate dob;
    private String gender;
    @ManyToOne
    private UserPermission role;
    private Boolean status;
    private String createBy;
    private LocalDate createDate;
    private String modifiedBy;
    private LocalDate modifiedDate;
}
