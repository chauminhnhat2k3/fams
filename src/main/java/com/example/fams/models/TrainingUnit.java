package com.example.fams.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tblTrainingUnit")
public class TrainingUnit {
    @Id
    private String unitCode;
    private String unitName;
    private Integer dayNumber;
    private String topicCode;
}
