package com.example.fams.repositories;

import com.example.fams.models.UserPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPermissionReposotory extends JpaRepository<UserPermission,Long> {
}
